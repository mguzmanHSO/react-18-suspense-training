import {createContext, useContext, useEffect, useState} from "react";
import {fetchCityDetailsData} from "@/data/fetchCityDetailsData";
import {CityListStoreContext} from "@/contexts/CityListStoreContext";

export const CityDetailsStoreContext = createContext(undefined);
const CityDetailsStoreProvider = ({initialCityId, children}) => {
	const {getCities} = useContext(CityListStoreContext);
	const cities = getCities();
	const [resourceCityDetails, setResourceCityDetails] = useState(fetchCityDetailsData(initialCityId));
	const setCityId = (cityId) => {
		setResourceCityDetails(fetchCityDetailsData(cityId));
	};

	useEffect(() => {
		setResourceCityDetails(fetchCityDetailsData(cities[0].id));
	},[cities])

	const contextValue = {
		setCityId,
		getCityInfo: resourceCityDetails?.cityInfo.read,
		getCityStats: resourceCityDetails?.cityStats.read,
		getCityLocation: resourceCityDetails?.cityLocation.read,
	};

	return (
		<CityDetailsStoreContext.Provider value={contextValue}>
			{children}
		</CityDetailsStoreContext.Provider>
	);
};

export {CityDetailsStoreProvider}
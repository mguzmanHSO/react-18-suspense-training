import {createContext, useContext, useEffect, useState, useTransition} from "react";
import {fetchCitiesDataResource} from "@/data/fetchCitiesDataResource";
import {DisplayCountContext} from "@/contexts/DisplayCountContext";

export const CityListStoreContext = createContext(undefined);
const CityListStoreProvider = ({children}) => {
	const {displayCount} = useContext(DisplayCountContext);
	const [isPending, startTransition] = useTransition();
	const [citiesDataResource, setCitiesDataResource] = useState(fetchCitiesDataResource(displayCount));

	useEffect(() => {
		startTransition(() => {
			setCitiesDataResource(fetchCitiesDataResource(displayCount))
		});
	}, [displayCount])

	const contextValue = {
		isPending,
		getCities: citiesDataResource?.cities.read
	};

	return (
		<CityListStoreContext.Provider value={contextValue}>
			{children}
		</CityListStoreContext.Provider>
	);
};

export {CityListStoreProvider}
import {useContext} from "react";
import {CityDetailsStoreContext} from "@/contexts/CityDetailsStoreContext";
import {Chip, Divider, ListItem, Typography} from "@mui/material";
import {CityDataList, CityDataListItem} from "@/components/styles";

const CityStats = () => {
	const {getCityLocation} = useContext(CityDetailsStoreContext);
	const data = getCityLocation();

	return (
		<CityDataList>
			<CityDataListItem sx={{display: 'flex', justifyContent: 'space-between'}}>
				<Typography sx={{paddingRight: '10px'}}>Latitude: </Typography>
				<Chip label={data.latitude}/>
			</CityDataListItem>
			<Divider orientation="vertical" flexItem/>
			<CityDataListItem>
				<Typography sx={{paddingRight: '10px'}}>Longitude: </Typography>
				<Chip label={data.longitude}/>
			</CityDataListItem>
		</CityDataList>
	);
}

export default CityStats;

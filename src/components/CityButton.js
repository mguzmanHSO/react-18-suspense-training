import {Button} from "@mui/material";
import React, {useContext} from "react";
import {CityDetailsStoreContext} from "@/contexts/CityDetailsStoreContext";

const CityButton = ({city}) => {
	const {setCityId} = useContext(CityDetailsStoreContext);
	return (
		<Button 
			onClick={
				(e) => {
					e.preventDefault();
					setCityId(city.id);
				}
			}
		>
			{city.name}
		</Button>
	);
};

export default CityButton;
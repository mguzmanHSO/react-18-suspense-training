import {useContext} from "react";
import {CityDetailsStoreContext} from "@/contexts/CityDetailsStoreContext";
import {Chip, Divider, ListItem, Typography} from "@mui/material";
import {CityDataList, CityDataListItem} from "@/components/styles";

const CityStats = () => {
	const {getCityStats} = useContext(CityDetailsStoreContext);
	const data = getCityStats();

	return (
		<CityDataList>
			<CityDataListItem>
				<Typography sx={{paddingRight: '10px'}}>Population: </Typography>
				<Chip label={data.population}/>
			</CityDataListItem>
			<Divider orientation="vertical" flexItem/>
			<CityDataListItem>
				<Typography sx={{paddingRight: '10px'}}>Growth 2010 - 2020: </Typography>
				<Chip label={data.growth}/>
			</CityDataListItem>
		</CityDataList>
	);
}

export default CityStats;

import styled from "styled-components";
import {Box} from "@mui/material";

export const CityListContainer = styled(Box)`
  display: flex;
  flex-direction: column;	  
  justify-content: center;
  border: 1px solid Grey;
  align-items: center;
  width: auto;
  background-color: white;
  margin: 8px;
`;

export const CityListDetailContainer = styled(Box)`
  display: flex;
  flex-direction: row;	  
  justify-content: center;
  border: 1px solid;
  align-items: center;
  border-radius: 10px;
  background-color: white;
  margin: 8px;
  width: auto;
`;
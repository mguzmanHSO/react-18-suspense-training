import {CityListHeader, ContentContainer, ListItemContainer, ListSearchItemContainer} from "@/pages/styles/styles";
import {Box, Grid, Input, List, ListItemIcon, Typography} from "@mui/material";
import CircleIcon from "@mui/icons-material/Circle";
import React, {useContext, useState} from "react";
import {CityListStoreContext} from "@/contexts/CityListStoreContext";
import {CityDetailsStoreProvider} from "@/contexts/CityDetailsStoreContext";
import {CityListContainer, CityListDetailContainer} from "@/components/CityList/styles";
import CityButton from "@/components/CityButton";
import CityListResults from "@/components/CityListResults";

const CityList = ({showDetails, children}) => {
	const {getCities, isPending} = useContext(CityListStoreContext);
	const [search, setSearch] = useState('')
	const cities = getCities();

	return (
		<CityDetailsStoreProvider initialCityId={cities[0].id} >
			<Box sx={{alignItems: 'center', display: 'flex', flexDirection: 'column'}}>
				<ContentContainer>
					<Typography
						variant={'h4'}
						sx={{fontWeight: 'bold',textDecoration: 'underline'}}
					>
					React No Suspense
					</Typography>
					<CityListDetailContainer>
						<Grid item sm={showDetails ? 3 : 12}>
							<CityListContainer>
								<List
									sx={{textAlign: 'center', padding: 0}}
									subheader={<CityListHeader>City List {isPending ? 'updating...' : ''}</CityListHeader>}
								>
									{!showDetails &&
										<>
											<ListSearchItemContainer>
												<Input
													id={'searchCityInput'}
													type={'search'}
													placeholder={'Search'}
													onChange={
														(e) => {setSearch(e.target.value);}
													}
												/>
											</ListSearchItemContainer>
											<ListSearchItemContainer>
												Search Text: {search}
											</ListSearchItemContainer>
										</>
									}
									<CityListResults cities={cities} searchText={search} useCityButton={showDetails} />
								</List>
							</CityListContainer>
						</Grid>
						{showDetails &&
							<Grid item sm={9}>
								{children}
							</Grid>
						}
					</CityListDetailContainer>
				</ContentContainer>
			</Box>
		</CityDetailsStoreProvider>
	);
};

export default CityList;
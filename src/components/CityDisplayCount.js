import {useContext} from "react";
import {DisplayCountContext} from "@/contexts/DisplayCountContext";
import {Button, ButtonGroup} from "@mui/material";

const CityDisplayCount = () => {
	const {displayCount, setDisplayCount} = useContext(DisplayCountContext);

	return (
		<ButtonGroup variant={"contained"}>
			{
				[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15].map((buttonCnt) => {
					return (
						<Button 
							type={"button"}
							color={displayCount === buttonCnt ? 'primary' : 'secondary'}
							key={buttonCnt} 
							onClick={
								() => {setDisplayCount(buttonCnt);}
							}
						>
							{buttonCnt}
						</Button>
					);
				})
			}
		</ButtonGroup>
	);
};

export default CityDisplayCount;
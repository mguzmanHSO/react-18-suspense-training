import {ListItemIcon} from "@mui/material";
import CircleIcon from "@mui/icons-material/Circle";
import CityButton from "@/components/CityButton";
import {ListItemContainer} from "@/pages/styles/styles";
import React from "react";

const CityListResults = ({cities, searchText, useCityButton}) => {
	return (
		<>
			{
				cities
					.filter(({name}) => {
						return (searchText.length === 0 || name.toLowerCase().includes(searchText.toLowerCase()))
					})
					.map(city => (
						<ListItemContainer key={city.id} >
							<ListItemIcon sx={{fontSize: '10px', minWidth:'15px', color: 'inherit'}}>
								<CircleIcon fontSize={'inherit'}/>
							</ListItemIcon>
							{useCityButton && <CityButton city={city} />}
							{!useCityButton && city.name}
						</ListItemContainer>
					))
			}
		</>
	);
};

export default CityListResults;
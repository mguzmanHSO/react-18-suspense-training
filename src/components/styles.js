import styled from "styled-components";
import {List, ListItem} from "@mui/material";

export const CityDataList = styled(List)`
  width: 100%;
  display: flex;
  list-style: none;
  border: solid 1px darkgray;
`;

export const CityDataListItem = styled(ListItem)`
  display: flex;
  justify-content: space-between;
`;
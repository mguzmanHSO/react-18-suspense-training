import styled from "styled-components";
import {Box} from "@mui/material";

export const CityDetailHeader = styled.h2`
  background-color: steelblue;
  width: 100%;
  text-align: center;
  height: 30px;
  margin: 0;
`;

export const CityDetailContainer = styled(Box)`
  display: flex;
  flex-direction: column;	  
  justify-content: center;
  border: 1px solid Grey;
  align-items: center;
  background-color: white;
  margin: 8px;
  width: 600px;
`;
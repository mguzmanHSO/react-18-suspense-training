import CityInfo from "@/components/CityInfo";
import CityStats from "@/components/CityStats";
import CityLocation from "@/components/CityLocation";
import {CityDetailContainer, CityDetailHeader} from "@/components/CityDetail/styles";
import React, {Suspense} from "react";

const CityDetail = () => {
	return (
		<CityDetailContainer>
			<CityDetailHeader>City Details</CityDetailHeader>
			<Suspense fallback={<h1>Loading...</h1>}>
				<CityInfo />
			</Suspense>
			<Suspense fallback={<h1>Loading...</h1>}>
				<CityStats />
			</Suspense>
			<Suspense fallback={<h1>Loading...</h1>}>
				<CityLocation />
			</Suspense>



		</CityDetailContainer>
	);
};

export default CityDetail;
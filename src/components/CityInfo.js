import {useContext} from "react";
import {CityDetailsStoreContext} from "@/contexts/CityDetailsStoreContext";
import {Chip, Divider, Typography} from "@mui/material";
import {CityDataList, CityDataListItem} from "@/components/styles";
import {BorderVertical} from "@mui/icons-material";

const CityInfo = () => {
	const {getCityInfo} = useContext(CityDetailsStoreContext);
	const data = getCityInfo();

	return (
		<CityDataList>
			<CityDataListItem>
				<Typography sx={{paddingRight: '10px'}}>Name:</Typography>
				<Chip label={data.name}/>
			</CityDataListItem>
			<Divider orientation="vertical" flexItem/>
			<CityDataListItem>
				<Typography sx={{paddingRight: '10px'}}>State:</Typography>
				<Chip label={data.state}/>
			</CityDataListItem>
		</CityDataList>
	);
}

export default CityInfo;

export default function handler(req, res) {
	const cities = [
		{
			id: 1,
			name: 'Rockford',
			state: 'Illinois',
			population: 147711,
			growth: "-2.7%",
			longitude: "-89.0940°",
			latitude: "42.2711°"
		},
		{
			id: 2,
			name: 'Chicago',
			state: 'Illinois',
			population: 2693976,
			growth: "2%",
			longitude: "-87623177°",
			latitude: "41.881832°"
		},
		{
			id: 3,
			name: 'Granite city',
			state: 'Illinois',
			population: 27500,
			growth: "-2.29%",
			longitude: "-90.139389°",
			latitude: "38.703732°"
		},
		{
			id: 4,
			name: 'Albuquerque',
			state: 'New Mexico',
			population: 562599,
			growth: "24.8%",
			longitude: "-106.629181°",
			latitude: "35.106766°"
		},
		{
			id: 5,
			name: 'Edgewood',
			state: 'New Mexico',
			population: 6154,
			growth: "210.0%",
			longitude: "-106.191383°",
			latitude: "35.061206°"
		},
		{
			id: 6,
			name: 'Santa Fe',
			state: 'New Mexico',
			population: 84683,
			growth: "1.75%",
			longitude: "-105.944183°",
			latitude: "35.691544°"
		},
		{
			id: 7,
			name: 'Tijeras',
			state: 'New Mexico',
			population: 465,
			growth: "4.59%",
			longitude: "-106.3878905°",
			latitude: "35.0795244°"
		},
		{
			id: 8,
			name: 'Belen',
			state: 'New Mexico',
			population: 7423,
			growth: "0.5%",
			longitude: "-106.776075°",
			latitude: "34.662809°"
		},
		{
			id: 9,
			name: 'Los Lunas',
			state: 'New Mexico',
			population: 17861,
			growth: "8.72%",
			longitude: "-106.733478°",
			latitude: "34.806642°"
		},
		{
			id: 10,
			name: 'Roswell',
			state: 'New Mexico',
			population: 47174,
			growth: "6.3%",
			longitude: "-104.5249100°",
			latitude: "33.3943700°"
		},
		{
			id: 11,
			name: 'Clovis',
			state: 'New Mexico',
			population: 37988,
			growth: "16.6%",
			longitude: "-103.2052272°",
			latitude: "34.4047987°"
		},
		{
			id: 12,
			name: 'Denver',
			state: 'Colorado',
			population: 715522,
			growth: "20.0%",
			longitude: "-104.991531°",
			latitude: "39.742043°"
		},
		{
			id: 13,
			name: 'Greeley',
			state: 'Colorado',
			population: 108649,
			growth: "1.47%",
			longitude: "-104.70776°",
			latitude: "40.422653°"
		},
		{
			id: 14,
			name: 'Boulder',
			state: 'Colorado',
			population: 108250,
			growth: "-3.07%",
			longitude: "-105.270546°",
			latitude: "40.014984°"
		}
	]
	return res.status(200).json(cities)
}
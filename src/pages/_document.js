import { Html, Head, Main, NextScript } from 'next/document'
import {Box} from "@mui/material";

export default function Document() {
	return (
		<Html lang="en">
			<Head/>
			<body>
				<Main />
				<NextScript/>
			</body>
		</Html>
	)
}
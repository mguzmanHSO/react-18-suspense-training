import CityDisplayCount from "@/components/CityDisplayCount";
import {CityListStoreProvider} from "@/contexts/CityListStoreContext";
import React, {Suspense} from "react";
import CityList from "@/components/CityList/CityList";
import {DisplayCountProvider} from "@/contexts/DisplayCountContext";

const CitySearch = () => {
	const displayCount = 100;
	return (
		<DisplayCountProvider initialDisplayCount={displayCount}>
			<CityDisplayCount />
			<CityListStoreProvider>
				<Suspense fallback={<h1>Loading...</h1>}>
					<CityList showDetails={false}/>
				</Suspense>
			</CityListStoreProvider>
		</DisplayCountProvider>
	);
}

export default CitySearch;
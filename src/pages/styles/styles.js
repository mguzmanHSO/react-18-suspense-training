import styled from "styled-components";
import {Box, ListItem} from "@mui/material";

export const ContentContainer = styled(Box)`
  display: flex;
  flex-direction: column;	  
  justify-content: space-between;
  border: 1px solid burlywood;
  align-items: center;
  width: auto;
  background-color: papayawhip;
  border-radius: 10px;
`;

export const ListItemContainer = styled(ListItem)`
  border: 1px solid Grey;
`;

export const ListSearchItemContainer = styled(ListItem)`
  border: 1px solid Grey;
  background-color: steelblue;
`;

export const CityListHeader = styled.h2`
  background-color: steelblue;
  height: 30px;
  margin: 0;
`;
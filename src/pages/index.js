import React, {Suspense} from 'react'
import CityList from "@/components/CityList/CityList";
import CityDisplayCount from "@/components/CityDisplayCount";
import {CityListStoreProvider} from "@/contexts/CityListStoreContext";
import CityDetail from "@/components/CityDetail/CityDetail";
import {DisplayCountProvider} from "@/contexts/DisplayCountContext";

const Home = () => {
	const displayCount = 3;
	return (
		<DisplayCountProvider initialDisplayCount={displayCount}>
			<CityDisplayCount />
			<CityListStoreProvider>
				<Suspense fallback={<h1>Loading...</h1>}>
					<CityList showDetails>
						<Suspense fallback={<h1>Loading...</h1>}>
							<CityDetail />
						</Suspense>
					</CityList>
				</Suspense>
			</CityListStoreProvider>
		</DisplayCountProvider>
	);
};

export default Home;

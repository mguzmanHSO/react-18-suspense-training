import APIUtil from "@/utils/APIUtil";

const mapCityStats = (city) => {
	return {
		id: city.id,
		population: city.population,
		growth: city.growth
	}
}

export const fetchCityStats = async (cityId) => {
	const filterByCityId = (data) => {
		return data.filter(d => d.id === cityId)
	}

	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(
				APIUtil
					.makeCall('Cities')
					.then(filterByCityId)
					.then(results => results.map(mapCityStats)[0])
			);
		},3100);
	})
}
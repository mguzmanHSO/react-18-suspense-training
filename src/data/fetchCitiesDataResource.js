import {fetchCities} from "@/data/Cities";
import {wrapPromise} from "@/utils/PromiseUtil";

export const fetchCitiesDataResource = (displayCount) => {
	return {
		cities: wrapPromise(fetchCities(displayCount))
	}
};
import APIUtil from "@/utils/APIUtil";

const mapCityLocation = (city) => {
	return {
		id: city.id,
		longitude: city.longitude,
		latitude: city.latitude
	}
}

export const fetchCityLocation = async (cityId) => {
	const filterByCityId = (data) => {
		return data.filter(d => d.id === cityId)
	}

	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(
				APIUtil
					.makeCall('Cities')
					.then(filterByCityId)
					.then(results => results.map(mapCityLocation)[0])
			);
		},1800);
	})
}
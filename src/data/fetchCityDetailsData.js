import {fetchCityInfo} from "@/data/CityInfo";
import {fetchCityStats} from "@/data/CityStats";
import {fetchCityLocation} from "@/data/CityLocation";
import {wrapPromise} from "@/utils/PromiseUtil";

export const fetchCityDetailsData = (cityId) => {
	return {
		cityInfo: wrapPromise(fetchCityInfo(cityId)),
		cityStats: wrapPromise(fetchCityStats(cityId)),
		cityLocation: wrapPromise(fetchCityLocation(cityId))
	}
};
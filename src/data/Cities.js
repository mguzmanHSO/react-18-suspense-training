import APIUtil from "@/utils/APIUtil";

const topCities = (cities, n) => {
	if (n === 0 || cities.length <= n) {
		return cities
			// .filter(c => c.state === 'New Mexico')
			.sort((a,b) => a.name.localeCompare(b.name))
			.sort((a,b) => parseInt(b.population) - parseInt(a.population));
	}
	return cities
		// .filter(c => c.state === 'New Mexico')
		.sort((a,b) => a.name.localeCompare(b.name))
		.slice(0,n)
		.sort((a,b) => parseInt(b.population) - parseInt(a.population));
};
export const fetchCities = async (displayCount) => {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(
				APIUtil
					.makeCall('Cities')
					.then(data => topCities(data, displayCount))
			);
		},1500);
	})
}
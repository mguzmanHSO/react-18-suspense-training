import APIUtil from "@/utils/APIUtil";

const mapCityInfo = (city) => {
	return {
		id: city.id,
		name: city.name,
		state: city.state
	}
}

export const fetchCityInfo = async (cityId) => {
	const filterByCityId = (data) => {
		return data.filter(d => d.id === cityId)
	}

	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(
				APIUtil
					.makeCall('Cities')
					.then(filterByCityId)
					.then(results => results.map(mapCityInfo)[0])
			);
		},2200);
	})
}
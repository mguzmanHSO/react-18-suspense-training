export const APIUtil = {
	makeCall: async (path, method = 'get') => {
		const initRequest = {
			method,
			headers: {
				accept: 'application/json',
				'Content-Type': 'application/json',
			},
		}

		return await fetch(`http://localhost:3001/api/${path}`, initRequest).then(resp => resp.json());
	}
};

export default APIUtil;
/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	crossOrigin: 'anonymous',
	compiler: {
		styledComponents: true
	}
}

module.exports = nextConfig
